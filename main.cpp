#include <iostream>
#include "AVLhfile.h"
#include "AVLmethods.h"

using namespace std;

int main()
{
     Dictionary<int,int> Dict;

      int someDictKeys[11] = {300,280,290,291,278,276,320,330,340,345,327};
      int someDictinfos[11] = {111,222,333,444,555,666,777,888,999,1010,1111};


     for(int m = 0;m<11;m++)
    {
        Dict.insert_in_dictionary(someDictKeys[m],someDictinfos[m]);
    }


    Dict.remove_from_dictionary(340);

    Dict.printDictionary();
    Dict.print_in_ascending_order();
    cout<<endl;
    Dict.print_preorder();
    cout<<endl;
    Dict.print_postorder();
    cout<<endl;
    cout<<Dict.Check_Key(340)<<endl;
    cout<<Dict.Check_Key(700)<<endl;
    cout<<endl;
    cout<<Dict.isEmpty()<<endl;
    cout<<Dict.DictionaryHeight()<<endl;
    cout<<Dict.how_many_leaves_in_dictionary()<<endl;
    cout<<endl;
    cout<<Dict.return_info_by_key(291)<<endl;

    Dictionary<int,int> Dict2;
    Dict2 = Dict;
    cout<<endl;
    //Dictionary2.printDictionary();

    Dictionary<string,string> Dict3;

    Dict3.insert_in_dictionary("Apple","fruit");
    Dict3.insert_in_dictionary("Tomato","vegetable");
    Dict3.insert_in_dictionary("C++","Programming language");
    Dict3.insert_in_dictionary("IT","Information Technologies");
    Dict3.insert_in_dictionary("Car","Road vehicle");
    Dict3.insert_in_dictionary("Helicopter","Military vehicle");
    Dict3.insert_in_dictionary("Wikipedia","Source for Information");
    Dict3.insert_in_dictionary("Linux","Operating System");
    Dict3.printDictionary();
    cout<<endl;
    cout<<Dict3.return_info_by_key("C++")<<endl;

    return 0;
}
