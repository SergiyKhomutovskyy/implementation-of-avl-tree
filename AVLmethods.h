#ifndef AVLMETHODS_H_INCLUDED
#define AVLMETHODS_H_INCLUDED

#include "AVLhfile.h"


/*.......PRIVATE METHODS DEFINITIONS BEGINS HERE......*/
template<typename Key,typename Info>
void  Dictionary<Key,Info> ::copyTree(Node* &copiedTreeRoot,Node* otherTreeRoot)
{
    if (otherTreeRoot ==NULL)
        copiedTreeRoot = NULL;
    else
    {
        copiedTreeRoot = new Node(otherTreeRoot->key,otherTreeRoot->info);
        copyTree(copiedTreeRoot->left, otherTreeRoot->left);
        copyTree(copiedTreeRoot->right, otherTreeRoot->right);
        //fixheight(copiedTreeRoot);
    }
} //end copyTree


template<typename Key,typename Info>
int Dictionary<Key,Info> :: max(int x,int y) const
    {
        if(x>=y)
            return x;
        else
            return y;

    }


template<typename Key,typename Info>
int Dictionary<Key,Info> :: height_of_the_tree(Node* pointer) const
    {
        if(pointer == NULL)
            return 0;
        else
        return 1 + max(height_of_the_tree(pointer->left),height_of_the_tree(pointer->right));

    }

template<typename Key,typename Info>
int Dictionary<Key,Info> :: countLeaves(Node* pointer)
    {
        if(pointer!= NULL)
        {
            //processing Node
            if(pointer->left == NULL && pointer->right == NULL)
            {
                Number_of_leaves_in_Tree++;
            }

            countLeaves(pointer->left);
            countLeaves(pointer->right);
        }
        return Number_of_leaves_in_Tree;
    }


template<typename Key,typename Info>
void Dictionary<Key,Info> :: in_order_traversal(Node* pointer) const
{


    if(root!=NULL) //if there is something in the tree
    {

        if(pointer->left!=NULL)
        {
            in_order_traversal(pointer->left); //going left if possible

        }

        //Processing the Node


        cout<<"Key: "<<pointer->key<<endl<<"Info: "<<pointer->info<<endl; //processing the node
        cout<<endl;

        if(pointer->right!=NULL)
        {
            in_order_traversal(pointer->right); //going right if possible

        }
    }
    else // if the tree is empty
    {
       cout<<"InOrder: The tree is empty"<<endl;
       return;
    }
}

template<typename Key,typename Info>
void Dictionary<Key,Info>  ::pre_order_traversal(Node* pointer) const
{


    if(root!=NULL) //if the tree is not empty
    {
        //processing the Node
        cout<<"Key: "<<pointer->key<<endl<<"Info: "<<pointer->info<<endl;
        cout<<endl;

        if(pointer->left!=NULL)  //going left if possible
        {
            pre_order_traversal(pointer->left);
        }

        if(pointer->right!=NULL) //going right i possible
        {
            pre_order_traversal(pointer->right);

        }
    }
    else
        {cout<<"PreOrder: The tree is empty"<<endl;
        return;}
}

template<typename Key,typename Info>
void Dictionary<Key,Info> :: post_order_traversal(Node* pointer) const
{

    if(root!=NULL)  // if the tree is not empty
    {
       if(pointer->left!=NULL) //going left if possible
        {
            post_order_traversal(pointer->left);
        }

        if(pointer->right!=NULL) //going right if possible
        {
            post_order_traversal(pointer->right);

        }

         //processing the Node
         cout<<"Key: "<<pointer->key<<endl<<"Info: "<<pointer->info<<endl;
         cout<<endl;
    }
    else
        {cout<<"PostOrder: The tree is empty"<<endl;
        return;}
}

template<typename Key,typename Info>
void Dictionary<Key,Info> :: destroy(Node* &pointer)
    {
          if(pointer!=NULL)
       {
           destroy(pointer->left);
           destroy(pointer->right);
           delete pointer;
           pointer = NULL;
       }

      }

template<typename Key,typename Info>
string Dictionary<Key,Info> :: indent(int indent) const
    {
        string out;

        for (int i = 0; i < indent; i++)
        {
            out += "  ";
        }

        return out;
    }


    // Print tree from Node n
template<typename Key,typename Info>
void Dictionary<Key,Info> :: print(Node *pointer, int step, string sign) const
    {
        if (pointer == NULL)
        {
            cout << indent(step) << "X";
            return;
        }

        print(pointer->right, step + 7, "(right) ");
        cout << endl;

        cout << indent(step) << sign << pointer->key<<endl;

        print(pointer->left, step + 7, "(left) ");
        cout << endl << indent(step);
    }

/*.......PRIVATE METHODS DEFINITIONS FINISHS HERE.....*/


//Public Methods:
template<typename Key,typename Info>
Dictionary<Key,Info>::Dictionary()
{
    root = NULL; // initialize the binary tree to an empty state
    cout<<"Default Constructor did his job"<<endl;
}

//copy constructor
template<typename Key,typename Info>
Dictionary<Key,Info>::Dictionary(const Node& otherTree)
{
    if (otherTree.root == NULL) //otherTree is empty
        root = NULL;
    else
        {copyTree(root, otherTree.root);
        cout<<"COPY CONSTRUCTOR DID HIS JOB"<<endl;}
}


     //Overload the assignment operator
template<typename Key,typename Info>
const Dictionary<Key,Info>& Dictionary<Key,Info> :: operator=(const Dictionary<Key,Info>& otherTree)
{
    if (this != &otherTree) //avoid self-copy
    {
        if (root != NULL)  //if the binary tree is not empty,
                           //destroy the binary tree
            destroy(root);

        if (otherTree.root == NULL) //otherTree is empty
            root = NULL;
        else
            {copyTree(root, otherTree.root);
            cout<<"ASSIGNMENT OPERATOR DID HIS JOB"<<endl;}
    }//end else

    return *this;
}

template<typename Key,typename Info>
void Dictionary<Key,Info> :: Destroy_dictionary()
{
    destroy(root);
}

template<typename Key,typename Info>
Dictionary<Key,Info>::~Dictionary()
{
    destroy(root);
    cout<<"Default Destructor did his job"<<endl;

}

template<typename Key,typename Info>
bool Dictionary<Key,Info> :: isEmpty()
{

    return(root == NULL);

}

template<typename Key,typename Info>
int Dictionary<Key,Info> :: DictionaryHeight()
{
    return height_of_the_tree(root);
}

template<typename Key,typename Info>
int Dictionary<Key,Info> :: how_many_leaves_in_dictionary()
{
    return countLeaves(root);
}

template<typename Key,typename Info>
bool Dictionary<Key,Info> :: Check_Key(const Key& searchKey )
{
    Node *current;
    bool found = false;

    if (root == NULL)
       {cout << "Cannot search the empty tree." << endl;
       return 0;}
    else
    {
        current = root;

        while (current != NULL && !found)
        {
            if (current->key == searchKey)
                found = true;
            else if (current->key > searchKey)
                current = current->left;
            else
                current = current->right;
        }//end while
    }//end else

    return found;
}//end search


template<typename Key,typename Info>
Info Dictionary<Key,Info>:: return_info_by_key(const Key& someKey)
{
    Node *current;
    bool found = false;

    if (root == NULL)
       {cout << "return_info_by_key : The tree is empty" << endl;
       return 0;}
    else
    {
        current = root;

        while (current != NULL && !found)
        {
            if (current->key == someKey)
                {found = true;
                return current->info;}
            else if (current->key > someKey)
                current = current->left;
            else
                current = current->right;
        }//end while
    }//end else

    return current->info;
}

template<typename Key,typename Info>
void Dictionary<Key,Info>:: insert_in_dictionary(const Key& someKey, const Info& someInfo)
{
        root = insertion(root, someKey, someInfo);
}

template<typename Key,typename Info>
void Dictionary<Key,Info>::remove_from_dictionary(const Key& removeKey)
{
        removing(root,removeKey);

}

template<typename Key,typename Info>
void Dictionary<Key,Info>::print_in_ascending_order()
{
    in_order_traversal(root);
}

template<typename Key,typename Info>
void Dictionary<Key,Info>:: print_preorder()
{
    pre_order_traversal(root);
}

template<typename Key,typename Info>
void Dictionary<Key,Info> :: print_postorder()
{
    post_order_traversal(root);
}

template<typename Key,typename Info>
void Dictionary<Key,Info>:: printDictionary()
 {
      if(root == NULL)

        {cout<<"printDictionary : The Tree is empty"<<endl;
        return;}

      int step = 0;
      string sign = "-> ";
     print(root,step,sign);

 }



#endif // AVLMETHODS_H_INCLUDED
