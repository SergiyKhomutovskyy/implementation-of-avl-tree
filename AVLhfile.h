#ifndef AVLHFILE_H_INCLUDED
#define AVLHFILE_H_INCLUDED

#include <string>
using namespace std;

template<typename Key,typename Info>
class Dictionary
{
private:

    // node
    struct Node
    {
        Key key;
        Info info;
        int height;
        Node *left;
        Node *right;

        // node constructor
        Node(const Key &someK, const Info &someI)
        {
            key = someK;
            info = someI;
            height = 1;
            left = NULL;
            right = NULL;
        }
    };

    Node* root;


    int Number_of_leaves_in_Tree = 0;


    int height(Node* pointer)
{
	if(pointer == NULL)
           return 0;
    else
        return pointer->height;
}

  //function which determines the balance factor of the given node
  //works only with non null pointers
  int bfactor(Node* pointer)
{
	return height(pointer->right) - height(pointer->left);
}

//function to correct the vale of height for the given Node  assuming left and right heights to be correct
//when to the given Node there is inserted either right either left child,hight of the opposite one - is zero
void fixheight(Node* pointer)
{
	int height_left = height(pointer->left);
	int height_right = height(pointer->right);
	pointer->height = (height_left > height_right ? height_left : height_right)+1;
}

//right rotation
//Function performs left rotation of a node
//The pointer of the node requiring the rotation ((the first Node violating the rules of Dictionary)
//is passed as a parameter to a function
Node* rotateright(Node* p)
{
	Node* q = p->left; //pointer q is now pointing to the left subtree of the root we want to rotate
	p->left = q->right; //right subtree of q is now left subtree of p
	q->right = p; //p is now the right subtree of q (rotation completed)
	//fixing heights
	fixheight(p);
	fixheight(q);
	return q;
}

//left rotation
Node* rotateleft(Node* q)
{
	Node* p = q->right; //pointer p is now pointing to the right subtree of the root we want to rotate
	q->right = p->left; //left subtree of p is now left subtree of q
	p->left = q; //q is now the left subtree of p (rotation completed)
	//fixing heights
	fixheight(q);
	fixheight(p);
	return p;
}

Node* balance(Node* p)
{
	fixheight(p);
	if( bfactor(p)== 2 ) //if right subtree is higher than left subtree
	{
		if( bfactor(p->right) < 0 ) //and if its left subtree is higher than the right subtree
			p->right = rotateright(p->right); //right left rotation is performed
		return rotateleft(p);
	}
	if( bfactor(p)== -2 )  //if left subtree is higher than the right subtree
	{
		if( bfactor(p->left) > 0  ) //and if in this left subtree its right subtree is higher
			p->left = rotateleft(p->left); //left right rotation is performed
		return rotateright(p);
	}

	return p; //if balancing is not needed
}


 // insert key, info in subtree with root n;
    Node* insertion(Node *ptr, const Key& someKey, const Info& someInfo)
    {
        if (ptr == NULL) //basically if the tree is empty
        {
            return new Node(someKey, someInfo);  //using parametrized constructor here
        }
        if (someKey < ptr->key)
        {
            ptr->left = insertion(ptr->left, someKey, someInfo);
        }
        else
        {
            ptr->right = insertion(ptr->right, someKey, someInfo);
        }

        return balance(ptr);  //balancing
    }


    // remove node with key someKey from a tree ptr
    //The Idea is the following:
    /*Frst,we find the Node ptr with the Key to delete
     //if such a key is not found than there is nothing to do
     //if it is found,than we search in its right subtree for the Node with the least key
     //and than we replace the Node we need to delete with the Node with the least key in its right subtree
     */

    Node* removing(Node *ptr, Key someKey)
    {
        if (ptr == NULL)
            {cout<<"Nothing to Delete"<<endl;
            return 0;}

        if (someKey < ptr->key)  //if Key is in the left subtree
        {
            ptr->left = removing(ptr->left,someKey);
        }

        else if (someKey > ptr->key) //if key is in the right subtree
        {
            ptr->right = removing(ptr->right,someKey);
        }

        else // if key is found
        {
            Node *left_node = ptr->left;   //memorizing roots of left and right subtree of the Node we want to delete from tree
            Node *right_node = ptr->right;
            delete ptr;  // Deleting the Node with the given Key

            if (right_node == NULL)
            {
                return left_node;  //by Dictionary tree properties,if the right subtree is empte
            }                      //than there is either one Node in the left subtree either left subtree is empty

            //if right subtree is not empty we search for the minimum key in it
            Node* minimum = find_min(right_node);
            minimum->right = remove_min(right_node); //attaching to the minimum element right->what is transformed from the right subtree
            minimum->left = left_node; //attaching left subtree of Node to be deleted to minimum key element
            return balance(minimum);
        }
        return balance(ptr);
    }


     //Function is to be useful whe removing particular Node
    // find node with min key in subtree with root n
    Node* find_min(Node *ptr)
    {
        return ptr->left ? find_min(ptr->left) : ptr;
    }

    // remove key with min value in subtree with root n
    //According to AVL properties the smallest element has either one Node at right either its empty
    // in both cases we only need to return  the pointer to the right Node of the smallest element
    Node* remove_min(Node *ptr)
    {
        if (ptr->left == NULL)
        {
            return ptr->right;
        }

        ptr->left = remove_min(ptr->left);

        return balance(ptr);
    }



    void copyTree(Node* &copiedTreeRoot,Node* otherTreeRoot);
      //Makes a copy of the binary tree to which
      //otherTreeRoot points. The pointer copiedTreeRoot
      //points to the root of the copied binary tree.


    int max(int x,int y) const;
    //simply returns the maximum number from two

    int height_of_the_tree(Node* pointer) const;

    int countLeaves(Node* pointer);
    //returns the number o leaves in the tree

    void in_order_traversal(Node* pointer) const;
    //recursively traverse the tree  in the following order:
    /*
    1.Go left if possible
    2.Process the current Node
    3.Go right if possible
    */


    void pre_order_traversal(Node* pointer) const;
    //recursively traverse the tree  in the following order:
    /*
    1.Process the current Node
    2.Go left if possible
    3.Go right if possible
    */

    void post_order_traversal(Node* pointer) const;
    //recursively traverse the tree  in the following order:
    /*

    1.Go left if possible
    2.Go right if possible
    3.Process the current Node
    */


    void destroy(Node* &pointer);
    //this function is to be used by DestroyTree function later
    //For each Node:
    /*First,we destroy its left subtree
    //Than its right sub tree
    //The the node itself*/
    // We pass a reference to pointer such that the function can modify the pointer passed to it
    // Because When we use "pass by pointer" to pass a pointer to a function,
    //Only a copy of the pointer is passed to the function. We can say "pass by pointer" is passing a pointer by value


    string indent(int indent) const;
    //Helper function to function print
    //makes indents


    void print(Node *pointer, int step, string sign) const;
    // Print tree from Node n


public:

    Dictionary();
    //default constructor
    //initialize the binary tree to an empty state,by setting
    //root = NULL;
     Dictionary(const Node& otherTree);
      //copy constructor
      const Dictionary& operator=(const Dictionary&);
      //Overload the assignment operator.
    ~Dictionary();
    //default destructor

    void print_in_ascending_order();
    void print_preorder();
    void print_postorder();

    // inserting
    void insert_in_dictionary(const Key& someKey, const Info& someInfo);

    void remove_from_dictionary(const Key& removeKey);

    void Destroy_dictionary();
    void RootOfDictionary() {cout<<"ROOT KEY: "<<root->key<<endl<<"ROOT INFO: "<<root->info<<endl;}
    void printDictionary();
    //int how_many_nodes_in_tree();
    int how_many_leaves_in_dictionary();
    bool isEmpty();
    int DictionaryHeight();
    bool Check_Key(const Key& searchKey);
      //Function to determine if the particular key is in the binary search tree.
      //Returns true if searchItem is found in the
      // binary search tree; otherwise, returns false.
    Info return_info_by_key(const Key& someKey);

};



#endif // AVLHFILE_H_INCLUDED
